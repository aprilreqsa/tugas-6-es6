import {sapa,convert,checkScore, filterData} from './lib/eslib'


const args = process.argv.slice(2)
const command = args[0]
switch (command) {
    case "sapa" :
        let nama = args [1]
        console.log(sapa(nama))
        break;
    case "convert" :
        let nami = args[1]
        let alamat = args[2]
        let umur = args[3]
        console.log(convert(nami,alamat,umur))
        break;
    case "checkScore" :
        let kata = args[1]
        console.log(checkScore(kata))
        break;
    case "filterData" :
        let namaKelas = args[1]
        console.log(filterData(namaKelas))
        break
    default: console.log("maaf data yang anda masukan salah")
    break;
}